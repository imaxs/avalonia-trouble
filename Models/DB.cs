using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalonia.NETCoreMVVM.Models
{
    public class Sql : DbContext
    {
        public DbSet<Entity> Entities { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(string.Format(@"Data Source=DB/DataBase.db"));
    }
    
    [Table( "TestTable")]
    public class Entity
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("Text")]
        public string Text { get; set; }
    }
}