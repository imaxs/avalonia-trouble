using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using System.Reactive.Disposables;
using Avalonia.NETCoreMVVM.ViewModels;
using ReactiveUI;

namespace Avalonia.NETCoreMVVM.Views
{
    public sealed class SearchView : ReactiveUserControl<SearchViewModel>
    {
        public SearchView()
        {
            this.WhenActivated(disposable => { });
            AvaloniaXamlLoader.Load(this);
        }
    }
}