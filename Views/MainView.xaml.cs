using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using System.Reactive.Disposables;
using Avalonia.NETCoreMVVM.ViewModels;
using ReactiveUI;

namespace Avalonia.NETCoreMVVM.Views
{
    public sealed class MainView : ReactiveWindow<Avalonia.NETCoreMVVM.ViewModels.MainViewModel>
    {
        public MainView()
        {
            this.WhenActivated(disposables => { });
            AvaloniaXamlLoader.Load(this);
        }
    }
}