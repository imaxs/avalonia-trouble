using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.NETCoreMVVM.ViewModels;
using Avalonia.NETCoreMVVM.Views;
using ReactiveUI;
using Splat;

namespace Avalonia.NETCoreMVVM
{
    public class App : Application
    {
        public override void Initialize() => AvaloniaXamlLoader.Load(this);

        public override void OnFrameworkInitializationCompleted()
        {
            Locator.CurrentMutable.RegisterConstant<IScreen>(new MainViewModel());
            Locator.CurrentMutable.Register<IViewFor<SearchViewModel>>(() => new SearchView());
            Locator.CurrentMutable.Register<IViewFor<LoginViewModel>>(() => new LoginView());
            
            new MainView { DataContext = Locator.Current.GetService<IScreen>() }.Show();
            base.OnFrameworkInitializationCompleted();
        }
    }
}