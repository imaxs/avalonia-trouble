using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Windows.Input;
using System.Reactive.Linq;
using System.Reactive;
using ReactiveUI;
using Splat;

namespace Avalonia.NETCoreMVVM.ViewModels
{
    public class SearchViewModel : ReactiveObject, IRoutableViewModel
    {
        private string _searchQuery;

        public SearchViewModel(IScreen screen = null) 
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
        }

        public IScreen HostScreen { get; }

        public string UrlPathSegment => "/search";

        public string SearchQuery 
        {
            get => _searchQuery;
            set => this.RaiseAndSetIfChanged(ref _searchQuery, value);
        }
    }
}