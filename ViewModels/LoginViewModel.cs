using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Reactive.Linq;
using System.Reactive;
using System.Threading;
using Avalonia.NETCoreMVVM.Models;
using ReactiveUI;
using Splat;

namespace Avalonia.NETCoreMVVM.ViewModels
{
    public class LoginViewModel : ReactiveObject, IRoutableViewModel
    {
        private string _password;
        private string _username;

        private ObservableCollection<Entity> _entities;
        
        private async Task MethodAsync()
        {
            await Task.Run(() =>
            {
                Console.WriteLine("Outer task starting...");
                Console.WriteLine("Outer task ThreadID: " + Thread.CurrentThread.ManagedThreadId);
                using var db = new Sql();
                _entities = new ObservableCollection<Entity>(db.Entities.ToList());
                Thread.Sleep(1500); // simulate long running task
                Username = _entities[0].Text;
                Console.WriteLine("Outer task finished");
            });
        }
        
        public LoginViewModel(IScreen screen = null) 
        {
            Console.WriteLine("Current ThreadID: " + Thread.CurrentThread.ManagedThreadId);
            HostScreen = Locator.Current.GetService<IScreen>();
            MethodAsync();
        }
        
        public IScreen HostScreen { get; }

        public string UrlPathSegment => "/login";

        public string Username 
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }
        
        public string Password 
        {
            get => _password;
            set => this.RaiseAndSetIfChanged(ref _password, value);
        }
    }
}